package Assignment1;
import java.util.*;
public class BankTest {

    public BankTest(){

    }

    public static void main(String[] args) {
        Bank bank = new Bank();
        Scanner input = new Scanner(System.in);

        String choice = "";
        while(!choice.equals("4")){

            System.out.println("\nWelcome to Merchants Bank \n" + "Select a task you want to perform :");
            System.out.println("*****************************************");

            System.out.println("1. Create a new account");
            System.out.println("2. Withdraw Cash");
            System.out.println("3. Deposit Cash");
            System.out.println("4.Exit");
            System.out.println("******************************************");

            System.out.println("Enter your choice");
            choice = input.next();

            try{
                if(choice.equals("1")){
                    bank.createAccount();
                }else if(choice.equals("2")){
                    bank.withdraw();
                }else if(choice.equals("3")){
                    bank.deposit();
                }else if(choice.equals("4")){
                    return;
                }else{
                    System.out.println("\nInvalid choice");
                }
            }catch(Exception e){
                System.out.println(e.getMessage());
            }

        }
    }
}
