package Assignment1;

import java.util.Scanner;
import java.util.InputMismatchException;
public class Bank {


   private int nextAccount = 0;
   private double getNextAccountNumber = 1;
   private final int maximumAccount = 5;
   private Account[] account;


   public Bank(){

       account = new Account[maximumAccount];
   }

   public void displayAccountDetails(Account accounts){

       System.out.println("\n Your Account details are :\n");
       System.out.format("%1614s   %26-25s    %36s\n", "Customer Name", "Account Number", " Account Balance ");
       System.out.println("------------------------------------ " + " -----------------------------");
       System.out.format("%16-14s  %26-15s     %36d\n", accounts.customerName, accounts.accountNumber, accounts.accountBalance);

   }

   public void createAccount(){
       Scanner input = new Scanner(System.in);

       try{
           System.out.println("\nEnter your Name/Company :");
           String customerName = input.nextLine();

          System.out.println("\nCreate your Account Number :");
          String accountNumber = input.next();

          System.out.println("\nEnter your customer's opening balance (dollars) : ");
          double accountBalance = input.nextDouble();

          if(accountBalance <= 100){
              throw new Exception("Your balance is not enough !!");
          }
       }catch(InputMismatchException e){
           System.out.println("Enter a non-zero positive number for balance.");
       }catch(Exception e){
           System.out.println(e.getMessage());
       }
   }

   public void withdraw(){
       Account accounts = new Account();
       Scanner input = new Scanner(System.in);
       try{
           System.out.println("Enter customer name :");
           String customerName = input.nextLine();



           System.out.println("Enter Customer account number :");
           String accountNumber = input.next();

           System.out.println("How much do you want to withdraw ?");
           double withdraw = input.nextDouble();

           if(withdraw <100){//toi thieu tren 100$
               throw new Exception("The money is not enough !!");
           }else if(withdraw > accounts.accountBalance){
               throw new InsufficientFundsException(withdraw);
           }else{

               accounts.accountNumber = getNextAccountNumber++;
               accounts.customerName = customerName;

               account[nextAccount++] = accounts;

           }
       }catch(ArrayIndexOutOfBoundsException e){
           System.out.println("Exception occurred - Could not withdraw money");
       }catch (InputMismatchException e){
           System.out.println("Exception occurred - " +e.getMessage());
       }catch(Exception e){
           System.out.println("Exception occurred - "+e.getMessage());
       }
   }


   public void deposit( ) {

       Scanner input = new Scanner(System.in);
       Account accounts = new Account();

       try {
           System.out.println("Enter customer name :");
           String customerName = input.nextLine();


           System.out.println("Enter Customer account number :");
           String accountNumber = input.next();

           System.out.println("How much do you want to deposit ?");
           double deposit = input.nextDouble();
       }catch(InputMismatchException e){
           System.out.println("Enter a non-zero positive number for balance.");
       }catch(Exception e){
           System.out.println(e.getMessage());
       }
   }

}

