package Assignment1;

public class InsufficientFundsException extends Exception{

    public InsufficientFundsException(){
        super();
    }

    public InsufficientFundsException(double withdraw){

        super("The withdraw " +withdraw +" is unavailable !");
    }
}
