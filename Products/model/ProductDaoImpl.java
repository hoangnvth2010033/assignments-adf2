package Products.model;
import Products.dao.DBConnection;
import Products.entity.Product;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductDaoImpl implements  ProductDao{

    private final Connection connection = DBConnection.createConnection();
    private final String SQL_CREATE_PRODUCT = " Insert into products(productName, productDesc, productPrice) values(?, ?, ?)";
    private final String SQL_GET_PRODUCT_BY_ID = "Select * from products where Id = ?";
    private final String SQL_GET_ALL_PRODUCTS = "Select * from products";
    private final String SQL_UPDATE_PRODUCT = "update products set productName = ?, productDesc = ?, productPrice = ? where Id = ?";
    private final String SQL_DELETE_PRODUCT = " DELETE from Products where Id = ?";

    @Override
    public void createProduct(Product product){
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_PRODUCT,Statement.RETURN_GENERATED_KEYS)){

            preparedStatement.setString(1, product.getProductName());
            preparedStatement.setString(2, product.getProductDesc());
            preparedStatement.setString(3,product.getProductPrice());
            preparedStatement.executeUpdate();
            try(ResultSet generatedKeys = preparedStatement.getGeneratedKeys()){
                if(generatedKeys.next()){
                    product.setProductId(generatedKeys.getInt(1));
                }
            }catch (SQLException ex){
                Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE,null,ex);
            }

        }
    }

    @Override
    public Product getProductById(int productId){
        Product product = new Product();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_PRODUCT_BY_ID)){
            preparedStatement.setInt(1,productId);
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()){
                    product.setProductId(resultSet.getInt(1));
                    product.setProductName(resultSet.getString(2));
                    product.setProductDesc(resultSet.getString(3));
                    product.setProductPrice(resultSet.getDouble(4));
                }
            }
        }catch (SQLException ex){
            Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE,null,ex);

        }
        return  product;
    }

    @Override
    public ArrayList<Product> getAllProducts(){
        ArrayList<Product> allProducts = new ArrayList<>();
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_ALL_PRODUCTS); ResultSet resultSet = preparedStatement.executeQuery()){
            while (resultSet.next()){
                Product product = new Product();
                product.setProductId(resultSet.getInt(1));
                product.setProductName(resultSet.getString(2));
                product.setProductDesc(resultSet.getString(3));
                product.setProductPrice(resultSet.getDouble(4));
                allProducts.add(product);
            }
        }catch (SQLException ex){
            Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE,null,ex);
        }
        return  allProducts;
    }

    @Override
    public void updateProduct(Product product){
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_PRODUCT)){
            preparedStatement.setString(1,product.getProductName());
            preparedStatement.setString(2,product.getProductDesc());
            preparedStatement.setDouble(3,product.getProductPrice());
              preparedStatement.setInt(8, product.getProductId());
              preparedStatement.executeUpdate();
        }catch (SQLException ex){
            Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE,null,ex);
        }
    }

    @Override
    public boolean deleteProduct(int productId){
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_PRODUCT)){
            preparedStatement.setInt(1,productId);
            preparedStatement.executeUpdate();
            return  true;
        }catch (SQLException ex){
            Logger.getLogger(ProductDaoImpl.class.getName()).log(Level.SEVERE,null,ex);

        }
        return  false;
    }
}
