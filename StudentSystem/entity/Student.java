package StudentSystem.entity;

public class Student {
    private String studentId;
    private String studentName;
    private String studentAddress;
    private String studentEmail;

    public String getId(){
        return studentId;
    }
    public void setId(String studentId){
        this.studentId = studentId;
    }
    public String getName(){
        return studentName;
    }
    public void setName(String studentName){
        this.studentName = studentName;
    }
    public String getAddress(){
        return studentAddress;
    }
    public void setAddress(String studentAddress){
        this.studentAddress = studentAddress;
    }
    public String getEmail(){
        return studentEmail;
    }
    public void setEmail(String studentEmail){
        this.studentEmail = studentEmail;
    }
}
