package StudentSystem.ui;
import StudentSystem.db.StudentAccessor;
import StudentSystem.entity.Student;


import java.util.*;

public class StudentManagement {

    StudentAccessor studentAccessor = new StudentAccessor();
    Scanner scan = new Scanner(System.in);
    Student student = new Student();
    public StudentManagement(){

    }

    public static void main(String[] args) {
        StudentAccessor studentAccessor = new StudentAccessor();
        StudentManagement studentManagement = new StudentManagement();
        Scanner input = new Scanner(System.in);
        Student student = new Student();

        while(true){
            System.out.println("\n------------------ MENU ---------------------");
            System.out.println(" Enter 1 : To add Student");
            System.out.println(" Enter 2 : To delete Student");
            System.out.println(" Enter 3 : To update Student");
            System.out.println(" Enter 4 : To search Student by Id");
            System.out.println(" Enter 5 : To display all Students ");
            System.out.println(" Enter 1 : To add Student");
            System.out.println(" Enter 1 : To add Student");
            System.out.println(" Enter 1 : To add Student");
            System.out.println(" Enter 0 : To exit");
                System.out.println("\nEnter your choice :");
                String choice = input.nextLine();
              switch (choice){
                  case "0":
                      System.out.println("\nThank you ! See you again !");
                      return;
                  case "1":
                      studentManagement.addStudent();
                      break;
                  case "2" :
                      studentManagement.delete();
                      break;
                  case "3" :
                      studentManagement.update();//chua duoc
                      break;
                  case "4" :
                      studentManagement.searchById();
                      break;
                  case "5" :
                      studentManagement.displayAll();
                      break;
                  default:
                      System.out.println(" Invalid choice !");
                      continue;

              }
        }
    }

    public void addStudent(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Student ID :");
        String studentId = scanner.next();
        System.out.println("Student Name :");
        String studentName = scanner.next();
        System.out.println("Student Address :");
        String studentAddress = scanner.next();
        System.out.println("Student Email :");
        String studentEmail = scanner.next();
        Student student = new Student();
        student.setName(studentName);
        student.setId(studentId);
        student.setAddress(studentAddress);
        student.setEmail(studentEmail);
        StudentAccessor studentAccessor = new StudentAccessor();
        try{
            studentAccessor.insert(student);
            System.out.println("\nInsert success !");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void delete(){
        System.out.println("Enter Student ID to delete :");
        String studentId = scan.next();
        try{
            studentAccessor.delete(studentId);
            System.out.println("\nDelete success !");
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void update(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Student ID to update :");
        String studentID = scanner.next();
        System.out.println("Enter new name of student "+studentID+" :");
        String newName = scanner.next();
        System.out.println("Enter new address of student "+studentID+" :");
        String newAddress = scanner.next();
        System.out.println("Enter new email of student "+studentID+" :");
        String newEmail = scanner.next();
         Student student = new Student();
         student.setName(newName);
        student.setName(newAddress);
        student.setName(newEmail);
         StudentAccessor studentAccessor = new StudentAccessor();
           try{
               studentAccessor.update(student);
           }catch (Exception ex){
               ex.printStackTrace();
           }
    }

    public void searchById(){

        System.out.println(" Student ID :");
        String studentID = scan.next();
        try{
            student = studentAccessor.findById(studentID);
            System.out.println(" Student ID : " +studentID+ " | Name : "+student.getName() +" | Address : "+student.getAddress()+" | Email : "+student.getEmail());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void displayAll(){
        try{
            System.out.println("---------- Show all students -----------");
            String name ="";
            List<Student> studentList = studentAccessor.getStudents(name);
            studentList.forEach(student ->{
                System.out.println(student.getId() + "\t\t" +student.getName() + "\t\t" +student.getAddress() + "\t\t" +student.getEmail() + "\t\t");
            } );
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
