package BT_Lab;

import java.io.*;
import java.util.*;
import java.util.Date;

public class ReadWriteObject {
    public static void main(String[] args) throws  IOException {
        Date currentDate = new Date();
        // write object

        ObjectOutputStream oos = null;
        try{
            oos = new ObjectOutputStream(
                    new BufferedOutputStream(
                            new FileOutputStream("object.dat")
                    )
            );
            oos.writeObject("The current Date and time is ");
            oos.writeObject(new Date());

            System.out.println("Object written !");
            oos.flush();
            oos.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }

        // read object
         ObjectInputStream ois = null;
        try{
            ois = new ObjectInputStream(new FileInputStream("object.dat"));
            Date d = (Date) ois.readObject();
            System.out.println("The data read from file :" + d);
            ois.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
