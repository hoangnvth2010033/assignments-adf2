package BT_Lab;
import java.util.*;
import java.io.*;
public class Student {

    private String name;
    private int age;
    private double mark;

    public Student(){

    }

    public Student(String name, int age, double mark){
        this.name = name;
        this.age = age;
        this.mark = mark;
    }

    public void setInformation(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Student name :");
        name = input.nextLine();

        do {
            System.out.println("Enter Student age:");
            age = input.nextInt();
            if(age <=0 ){
                System.out.println("\nStudent's age is invalid. ");
            }
        }while(age <= 0);

        do {
            System.out.println("Enter Student mark :");
            mark = input.nextDouble();
            if(mark <0){
                System.out.println("\nStudent's mark is invalid .");
            }
        }while(mark <0 );

        this.saveState();
        System.out.println("\n Add new student's information DONE !");
    }

    public void saveState() {

        try{
            FileWriter fileWriter = new FileWriter("D:\\epc\\Ap-Sem2\\ADF2\\Collection\\src\\BT_Lab\\Student.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(this.name);
            bufferedWriter.write("\t");
            bufferedWriter.write(Integer.toString(this.age));
            bufferedWriter.write("\t");
            bufferedWriter.write(Double.toString(this.mark));
            bufferedWriter.write("\t");
            bufferedWriter.newLine();
            bufferedWriter.close();
            fileWriter.close();
        }catch(FileNotFoundException f){
            f.printStackTrace();
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public void display(){
        try{
            FileReader fileReader = new FileReader("D:\\epc\\Ap-Sem2\\ADF2\\Collection\\src\\BT_Lab\\Student.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            fileReader.close();
        }catch(FileNotFoundException f){
            f.printStackTrace();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
